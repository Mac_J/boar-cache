package com.boarsoft.boar.cache;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.common.Util;

public class CacheConfig {
	private static final Logger log = LoggerFactory.getLogger(CacheConfig.class);
	private static final Properties prop = new Properties();

	static {
		try {
			log.info("Load boar-cache config.properties ...");
			prop.load(CacheConfig.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			log.error("Error on load config.properties.", e);
		}
	}

	public static final int AGENTX_PORT = getInt("agentx.port", 9925);

	public static int getInt(String key, int value) {
		return Util.str2int(prop.getProperty(key), value);
	}
}
