package com.boarsoft.boar.cache.db.action;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.LogonI;
import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.common.Constants;
import com.boarsoft.cache.redis.RedisNodeAgent;
import com.boarsoft.common.Util;
import com.boarsoft.web.login.Logined;

@RestController
@RequestMapping("/node")
public class CacheNodeAction {
	private static final Logger log = LoggerFactory.getLogger(CacheNodeAction.class);

	@Autowired
	private RedisNodeAgent redisAgent;

	@RequestMapping("/batch.do")
	public ReplyInfo<Object> batch(@Logined LogonI<String> logon, File file, String host, int port, String cmd) {
		if (file == null) {
			return Constants.REPLY_WARN_INVALID;
		}
		String to = String.format("redis/%s/batch.tmp", logon.getCode());
		try {
			return redisAgent.batch(file, host, port, to, cmd);
		} catch (Exception e) {
			log.error("Error on get info of cache node {}:{}", host, port, e);
			return Constants.REPLY_ERROR_INTERNAL;
		}
	}

	@RequestMapping("/exec.do")
	public ReplyInfo<Object> exec(String host, int port, String cmd) {
		if (Util.strIsEmpty(cmd)) {
			return Constants.REPLY_WARN_INVALID;
		}
		try {
			return redisAgent.exec(host, port, cmd);
		} catch (Exception e) {
			log.error("Error on get info of cache node {}:{}", host, port, e);
			return Constants.REPLY_ERROR_INTERNAL;
		}
	}

	/**
	 * 缓存节点信息
	 */
	@RequestMapping("/info.do")
	public ReplyInfo<Object> info(String host, int port) {
		try {
			return redisAgent.getInfo(host, port);
		} catch (Exception e) {
			log.error("Error on get info of cache node {}:{}", host, port, e);
			return Constants.REPLY_ERROR_INTERNAL;
		}
	}

	@RequestMapping("/start.do")
	public ReplyInfo<Object> start(String master, String host, int port) {
		try {
			if (Util.strIsEmpty(master)) {
				// 作为master启动
				return redisAgent.startup(host, port);
			}
			// 作为指定master节点的slave启动
			return redisAgent.startup(host, port, master);
		} catch (Exception e) {
			log.error("Error on start cache node {}:{}", host, port, e);
			return Constants.REPLY_ERROR_INTERNAL;
		}
	}

	@RequestMapping("/shutdown.do")
	public ReplyInfo<Object> shutdown(String host, int port) {
		try {
			return redisAgent.shutdown(host, port);
		} catch (Exception e) {
			log.error("Error on shutdown cache node {}:{}", host, port, e);
			return Constants.REPLY_ERROR_INTERNAL;
		}
	}

	@RequestMapping("/master.do")
	public ReplyInfo<Object> master(String host, int port) {
		try {
			// 将当前节点设置为master
			return redisAgent.toMaster(host, port);
		} catch (Exception e) {
			log.error("Error on set cache node {}:{} to master", host, port, e);
			return Constants.REPLY_ERROR_INTERNAL;
		}
	}

	@RequestMapping("/slave.do")
	public ReplyInfo<Object> slave(String host, int port, String master) {
		try {
			// 将当前节点设置为指定master的slave
			return redisAgent.toSlave(host, port, master);
		} catch (Exception e) {
			log.error("Error on set cache node {}:{} to slave of {}", //
					host, port, master, e);
			return Constants.REPLY_ERROR_INTERNAL;
		}
	}
}