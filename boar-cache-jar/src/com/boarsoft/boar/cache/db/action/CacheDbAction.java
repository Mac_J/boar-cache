package com.boarsoft.boar.cache.db.action;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.cache.bean.CacheTreeNode;
import com.boarsoft.boar.common.TreeNode;
import com.boarsoft.cache.config.CacheCatalog;
import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.common.util.JsonUtil;

@RestController
@RequestMapping("/db")
public class CacheDbAction {
	@Autowired
	protected CacheCatalog cacheCatalog;

	/**
	 * 查询所有分库，及其分片信息
	 */
	@RequestMapping("/list")
	public ReplyInfo<Object> list() {
		List<TreeNode> lt = new LinkedList<TreeNode>();
		Map<String, CacheDb> dm = cacheCatalog.getDbMap();
		if (dm != null) {
			for (CacheDb db : dm.values()) {
				lt.add(new CacheTreeNode(db, "root", ""));
			}
		}
		return new ReplyInfo<Object>(true, JsonUtil.from(lt));
	}
}
