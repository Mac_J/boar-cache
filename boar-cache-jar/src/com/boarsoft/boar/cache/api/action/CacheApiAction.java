package com.boarsoft.boar.cache.api.action;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.cache.bean.CacheItem;
import com.boarsoft.boar.common.Constants;
import com.boarsoft.cache.Cache;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.common.util.JsonUtil;

@RestController
@RequestMapping("/api")
public class CacheApiAction {
	private static final Logger log = LoggerFactory.getLogger(CacheApiAction.class);
	/** 注入来自远程缓存监控的目录服务 */
	@Autowired
	private Cache cache;

	@RequestMapping("/list")
	public ReplyInfo<Object> list(String db, String group, int pageNo, int pageSize) {
		List<CacheItem> lt = new LinkedList<CacheItem>();
		try {
			Map<String, Object> rm = cache.list(db, group);
			for (String k : rm.keySet()) {
				long ttl = cache.ttl(group, k);
				CacheItem o = new CacheItem();
				o.setGroup(group);
				o.setKey(k);
				o.setTtl(ttl);
				o.setValue(rm.get(k));
				lt.add(o);
			}
			PagedResult<CacheItem> pr = new PagedResult<CacheItem>(lt.size(), lt, pageNo, pageSize);
			return new ReplyInfo<Object>(true, JsonUtil.from(pr));
		} catch (Exception e) {
			log.error("Error on list {}/{}", db, group, e);
			return new ReplyInfo<Object>(false, Constants.ERROR_INTERNAL);
		}
	}

	@RequestMapping("/put")
	public ReplyInfo<Object> put(String group, String key, String value, int expire) {
		try {
			cache.put(group, key, value, expire);
			return ReplyInfo.SUCCESS;
		} catch (Exception e) {
			log.error("Error on put {}/{} = {}", group, key, value, e);
			return new ReplyInfo<Object>(false, Constants.ERROR_INTERNAL);
		}
	}

	@RequestMapping("/get")
	public ReplyInfo<Object> get(String group, String key) {
		try {
			Object v = cache.get(group, key);
			return new ReplyInfo<Object>(true, JsonUtil.toJSONString(v));
		} catch (Exception e) {
			log.error("Error on get {}/{}", group, key, e);
			return new ReplyInfo<Object>(false, Constants.ERROR_INTERNAL);
		}
	}

	@RequestMapping("/remove")
	public ReplyInfo<Object> remove(String group, String key) {
		try {
			cache.remove(group, key);
			return ReplyInfo.SUCCESS;
		} catch (Exception e) {
			log.error("Error on remove {}/{}", group, key, e);
			return new ReplyInfo<Object>(false, Constants.ERROR_INTERNAL);
		}
	}
}
