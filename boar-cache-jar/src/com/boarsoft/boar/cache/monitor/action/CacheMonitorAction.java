package com.boarsoft.boar.cache.monitor.action;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.cache.config.CacheCatalog;
import com.boarsoft.cache.config.CacheNode;
import com.boarsoft.cache.monitor.CacheMonitor;
import com.boarsoft.common.util.JsonUtil;

@RestController
@RequestMapping("/monitor")
public class CacheMonitorAction {
	private static final Logger log = LoggerFactory.getLogger(CacheMonitorAction.class);

	@Autowired
	protected CacheMonitor cacheMonitor;
	@Autowired
	protected CacheCatalog cacheCatalog;

	/**
	 * 通知CacheMonitor切换主从节点
	 */
	@RequestMapping("/switch2")
	public ReplyInfo<String> switch2(HttpServletRequest req) {
		String seqNo = req.getParameter("seqNo");
		String redisDb = req.getParameter("redisDb");
		String shardIndexS = req.getParameter("shardIndex");
		String switchMaster = req.getParameter("switchMaster");

		Map<String, String> data = new LinkedHashMap<String, String>();
		data.put("respCode", "0000000000");
		try {
			int shardIndex = 0;
			try {
				shardIndex = Integer.parseInt(shardIndexS);
			} catch (Exception e) {
				throw new RuntimeException("分片序号错误:" + shardIndexS);
			}
			CacheNode to = cacheCatalog.getCacheNode(redisDb, shardIndex, switchMaster);
			cacheMonitor.switch2(redisDb, shardIndex, to);
			log.info("Cache monitor switch master of {} shard {} to {} successfully", //
					redisDb, shardIndex, switchMaster);
		} catch (Exception e) {
			data.put("respCode", "CACHE000003");
			data.put("info", "seqNo:" + seqNo + ", error:" + e.getMessage());
		}

		String jsonData = JsonUtil.from(data);
		log.info("switch2 end seqNo:{}, jsonData:{}", seqNo, jsonData);
		return new ReplyInfo<String>(true, jsonData);
	}
}
