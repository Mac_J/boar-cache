package com.boarsoft.boar.cache.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.common.util.JsonUtil;

public class CacheItem {
	private static final Logger log = LoggerFactory.getLogger(CacheItem.class);
	
	protected String group;
	protected String key;
	protected String type;
	protected Object value;
	protected long ttl;

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		if (value == null) {
			this.value = null;
			return;
		}
		this.type = value.getClass().getName();
		try {
			this.value = JsonUtil.from(value);
		} catch (Exception e) {
			this.value = e.getMessage();
			log.warn("Can not convert {} to JSON", value, e);
		}
	}

	public long getTtl() {
		return ttl;
	}

	public void setTtl(long ttl) {
		this.ttl = ttl;
	}
}
