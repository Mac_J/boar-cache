package com.boarsoft.boar.cache.bean;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.boarsoft.boar.common.TreeNode;
import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.config.CacheNode;
import com.boarsoft.cache.config.CacheShard;

public class CacheTreeNode implements TreeNode {
	private static final short TYPE_DB = 0;
	private static final short TYPE_SHARD = 1;
	private static final short TYPE_NODE = 2;

	protected String key;
	protected int childCount;
	protected short type = 0;
	protected List<TreeNode> children = new LinkedList<TreeNode>();
	protected String path;
	protected String parent;
	protected String flag;
	protected short status;
	
	protected Map<String, Object> data = new HashMap<String, Object>();

	public CacheTreeNode(CacheDb db, String parent, String pp) {
		this.type = TYPE_DB;
		this.key = db.getCode();
		this.parent = parent;
		this.path = String.format("%s/%s", pp, key);
		this.flag = db.getClass().getName();
		List<CacheShard> csLt = db.getShards();
		this.childCount = csLt.size();
		for (CacheShard cs : csLt) {
			children.add(new CacheTreeNode(cs, key, path));
		}
	}

	public CacheTreeNode(CacheShard cs, String parent, String pp) {
		this.type = TYPE_SHARD;
		this.key = String.format("%02d", cs.getIndex());
		this.parent = parent;
		this.path = String.format("%s/%s", pp, key);
		this.flag = String.valueOf(cs.getStandby());
		this.status = cs.getStatus();
		Map<String, CacheNode> cnMap = cs.getBackups();
		this.childCount = cnMap.size();
		CacheNode m = cs.getMaster();
		if (m != null) {
			children.add(new CacheTreeNode(m, key, path, true));
		}
		for (CacheNode cn : cnMap.values()) {
			children.add(new CacheTreeNode(cn, key, path, false));
		}
	}

	public CacheTreeNode(CacheNode cn, String parent, String pp, boolean isMaster) {
		this.type = TYPE_NODE;
		this.key = cn.getCode();
		this.parent = parent;
		this.path = String.format("%s/%s", pp, key);
		this.flag = isMaster ? "0" : "1";
		this.status = cn.getStatus();
		//
		data.put("host", cn.getHost());
		data.put("port", cn.getPort());
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public String getTitle() {
		return key;
	}

	@Override
	public void setTitle(String title) {
		// this.key = title;
	}

	@Override
	public int getChildCount() {
		return childCount;
	}

	@Override
	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	@Override
	public boolean getIsFolder() {
		return childCount > 0;
	}

	@Override
	public void setIsFolder(boolean b) {
		// Nothing to do
	}

	@Override
	public boolean getIsLazy() {
		return false;
	}

	@Override
	public void setIsLazy(boolean b) {
		// Nothing to do
	}

	@Override
	public int getLevel() {
		return type;
	}

	@Override
	public void setLevel(int level) {
		// Nothing to do
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String getParent() {
		return parent;
	}

	@Override
	public void setParent(String parent) {
		this.parent = parent;
	}

	@Override
	public List<TreeNode> getChildren() {
		return children;
	}

	@Override
	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}
}
