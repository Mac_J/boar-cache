package com.boarsoft.boar.cache.event;

import java.util.HashMap;
import java.util.Map;

import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.boarsoft.boar.cache.bean.CacheTreeNode;
import com.boarsoft.cache.config.CacheCatalog;
import com.boarsoft.cache.config.CacheDb;
import com.boarsoft.cache.event.CacheEvent;
import com.boarsoft.cache.event.CacheEventSvc;
import com.boarsoft.web.websocket.WebsocketEndpoint;
import com.boarsoft.web.websocket.WebsocketMessage;

@Component("cacheEventSvc")
public class CacheEventSvcImpl implements CacheEventSvc {
	private static final Logger log = LoggerFactory.getLogger(CacheEventSvcImpl.class);
	/** */
	@Lazy
	@Autowired
	protected CacheCatalog catalog;

	@Override
	public void on(CacheEvent ce) {
		log.info("Received cache event: {} from {}", ce.getCode(), ce.getFrom());
		//
		WebsocketMessage mw = new WebsocketMessage();
		mw.setGroup("cache");
		mw.setCode("event");
		//
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("key", ce.getKey());
		map.put("code", ce.getCode());
		map.put("db", ce.getDbCode());
		Map<String, CacheDb> dm = catalog.getDbMap();
		if (dm != null) {
			CacheDb db = dm.get(ce.getDbCode());
			map.put("data", new CacheTreeNode(db, "root", ""));
		}
		mw.setData(map);
		//
		Map<String, Session> sm = WebsocketEndpoint.getSessionMap();
		for (String sid : sm.keySet()) {
			WebsocketEndpoint.send(mw, sid);
		}
	}
}
