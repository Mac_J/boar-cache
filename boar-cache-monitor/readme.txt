vim /etc/sysconfig/network-scripts/ifcfg-ens33
-----------------------------------------------------
BOOTPROTO="static"

IPADDR="192.168.226.128"
NETMASK="255.255.255.0"
GATEWAY="192.168.226.2"
DNS1="192.168.226.2"
-----------------------------------------------------

service network restart


cd ~/redis
sh startMaster.sh 0.0.0.0 6479
sh startSlave.sh 0.0.0.0 6481 127.0.0.1 6479
sh startMaster.sh 0.0.0.0 6480
sh startSlave.sh 0.0.0.0 6482 127.0.0.1 6480
sh startMaster.sh 0.0.0.0 6483
sh startMaster.sh 0.0.0.0 6484
cd ~

rm ./redis1/nodes.conf
rm ./redis1/dump.rdb
rm ./redis2/nodes.conf
rm ./redis2/dump.rdb
rm ./redis3/nodes.conf
rm ./redis3/dump.rdb
rm ./redis4/nodes.conf
rm ./redis4/dump.rdb
rm ./redis5/nodes.conf
rm ./redis5/dump.rdb
rm ./redis6/nodes.conf
rm ./redis6/dump.rdb

cd ~/redis1/
sh startMaster.sh
cd ~/redis2
sh startMaster.sh
cd ~/redis3
sh startMaster.sh
cd ~/redis4
sh startMaster.sh
cd ~/redis5
sh startMaster.sh
cd ~/redis6
sh startMaster.sh
cd ~

./redis/bin/redis-cli -p 6381 cluster reset
./redis/bin/redis-cli -p 6382 cluster reset
./redis/bin/redis-cli -p 6383 cluster reset
./redis/bin/redis-cli -p 6384 cluster reset
./redis/bin/redis-cli -p 6385 cluster reset
./redis/bin/redis-cli -p 6386 cluster reset
./redis/bin/redis-cli -p 6381 --cluster create --cluster-replicas 1 192.168.226.128:6381 192.168.226.128:6382 192.168.226.128:6383 192.168.226.128:6384 192.168.226.128:6385 192.168.226.128:6386


