package com.boarsoft.boar.cache.monitor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CacheMonitorConfig {
	private final static Logger log = LoggerFactory.getLogger(CacheMonitorConfig.class);
	private static Properties prop;

	static {
		if (prop == null) {
			InputStream is = CacheMonitorConfig.class.getClassLoader().getResourceAsStream("conf/config.properties");
			if (is == null) {
				log.warn("Can not init CacheMonitorConfig/config.properties");
			} else {
				log.info("Init CacheMonitorConfig with default config.properties");
				CacheMonitorConfig.init(is);
			}
		}
	}

	public static void init(InputStream is) {
		if (prop == null) {
			prop = new Properties();
			try {
				prop.load(is);

			} catch (IOException e) {
				log.error("Error on load system config.", e);
				throw new RuntimeException(e);
			}
		}
	}

	public static String getProperty(String name) {
		return prop.getProperty(name);
	}
}