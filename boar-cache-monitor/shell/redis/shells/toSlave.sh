#!/bin/bash
########################
# toSlave.sh 主从切换脚本
# 使用redis的客户端redis-cli远程切换主从
########################
#cd /home/app/deploy/redis
#./redis-cli -h $1 -p $2 slaveof $3 $4

ssh redis@$1 "~/redis-3.2.9/src/redis-cli -h $1 -p $2 slaveof $3 $4"
exit 0

