#!/bin/bash

. ./env.conf

echo "##########deploy.sh start##########"

sh stop.sh
is_suc "exec stop.sh"

sh update.sh
is_suc "exec update.sh"

sh start.sh
is_suc "exec start.sh"


echo "##########deploy.sh SUC##########"
exit 0

