#!/bin/bash
#$1: 编译目录(非空)
#$2: 编译环境(非空)

. ./env.conf

print_err() 
{
	echo "input error"
	echo "please input: sh package.sh \$1 \$2"
	echo "\$1: source path"
	echo "\$2: compile environment <CIT/UATA/UATB/PRD/...>"
	echo ""
	
	return 0
}

echo "package.sh start"

if [ $# -lt 2 ]; then
	print_err
	exit 1
fi

src_path=${SRC_PRE_FIX_PATH}/$1
cpl_env=$2
if [ ! -d ${src_path} ]; then
	echo "[ERROR]: source path [${src_path}] not found"
	exit 1
fi

cd ${src_path}
is_suc "cd ${src_path}"

#for kf
rm -r /home/nlzx/repository/com/boarsoft/mac-*
is_suc "for kf: [rm -r /home/nlzx/repository/com/boarsoft/mac-*]"

echo "[INFO]: star mvn -U clean install"
ret=`mvn -U clean install -Dmaven.test.skip=true -Dmaven.test.failture.ignore=true -P${cpl_env}`
if [[ $? -ne 0 ]]; then
	echo "[ERROR]: command [mvn -U clean install -P${cpl_env}] is failed!!!"
	echo $ret |sed 's/\[/\n\[/g' |awk '/ERROR/ {print}' 
	exit 1
else
	echo "[INFO]: command [mvn -U clean install -P${cpl_env}] is success"
fi
#echo "${JAVA_HOME}"
#mvn -U clean install -Dmaven.test.skip=true -Dmaven.test.failture.ignore=true -P${cpl_env}


cd target
is_suc "cd target"

zip -q -r ${APP_NAME}.zip ${APP_NAME}.jar conf lib shell sbin

cur_pwd=`pwd`
is_suc "pwd"

ret=${cur_pwd}/${APP_NAME}.zip
echo "success, ${ret}"

exit 0

