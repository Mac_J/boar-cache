package com.boarsoft.boar.cache.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.boarsoft.cache.Cache;

public class Main {
	private static ClassPathXmlApplicationContext ctx;

	public static void main(String[] args) throws Exception {
		ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));

		Cache cache = (Cache) ctx.getBean("redisCache");
		for (int i = 0; i < 1; i++) {
			String s = "a".concat(String.valueOf(i));
			cache.put("token", s, s);
		}
		for (int i = 0; i < 1; i++) {
			String s = "a".concat(String.valueOf(i));
			System.out.println(cache.get("token", s));
		}
		
		cache.put("test", "hello", "hello");
		System.out.println(cache.get("test", "hello"));
	}
}
