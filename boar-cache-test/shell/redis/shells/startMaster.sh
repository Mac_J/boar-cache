log="/home/redis/logs/redis/$1"
nohup ~/redis-3.2.9/src/redis-server --port $1 --protected-mode no 1>"$log".out 2>"$log".err &
exit 0
