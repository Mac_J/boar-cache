#!/bin/bash

. ./env.conf


is_stop()
{
	if [ "$1" == "" ]; then
		echo "pid is null, please input process id"
		return 1 
	fi

	local cur_num
	local pid=$1
	for ((i=0; i<${APP_SLEEP_NUM}; i++))
	do
		cur_num=1
		#cur_num=`ps -ef |grep "$RUN_COMMAND" |grep "$1" |grep -v "grep" |wc -l`
		cur_num=`ps -ef |awk '$2=='"$pid"' {print}' |wc -l`
		is_suc "ps -ef |awk 'pid==$pid {print}' |wc -l"
		if [ ${cur_num} -eq 0 ]; then
			echo "pid[${pid}] is stopped"
			return 0
		fi

		echo "stop [${APP_SLEEP_TIME}], please wait..."
		sleep ${APP_SLEEP_TIME} 
	done

	echo "sleep ${APP_SLEEP_NUM}*${APP_SLEEP_TIME} second, pid[$1] unable to stop, please check !!!"
	return 1
}
 
pids=`ps -ef|grep "$RUN_COMMAND" | grep -v "grep"|awk '{print $2}'`
if [ "$pids" = "" ]; then
	echo "[INFO]: Application[$APP_NAME] does not started !!"
else
	for pid in ${pids}; do
		kill ${pid} 1>/dev/null 2>&1
		is_stop ${pid}
 		is_suc "stop Application[$APP_NAME](pid=${pid})"
	done
fi


exit 0

