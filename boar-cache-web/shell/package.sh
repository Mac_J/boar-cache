#!/bin/bash
#$1: 编译目录(非空)
#$2: 编译环境(非空)

. ./env.conf

print_err() 
{
	echo "input error"
	echo "please input: sh package.sh \$1 \$2"
	echo "\$1: source path"
	echo "\$2: compile environment <CIT/UATA/UATB/PRD/...>"
	echo ""
	
	return 0
}

echo "package.sh start"

if [ $# -lt 2 ]; then
	print_err
	exit 1
fi

src_path=${SRC_PRE_FIX_PATH}/$1
cpl_env=$2
if [ ! -d ${src_path} ]; then
	echo "[ERROR]: source path [${src_path}] not found"
	exit 1
fi

cd ${src_path}
is_suc "cd ${src_path}"

mvn -U clean install -Dmaven.test.skip=true -Dmaven.test.failture.ignore=true -P${cpl_env}
is_suc "mvn -U clean install -P${cpl_env}"

cd target
is_suc "cd target"

cp -r ../${SBIN_DIR_NAME} ./
is_suc "cp -r ../${SBIN_DIR_NAME} ./"

zip -q -r ${APP_ZIP_NAME} ${SBIN_DIR_NAME} ${APP_NAME}
is_suc "zip -q -r ${APP_ZIP_NAME} ${SBIN_DIR_NAME} ${APP_NAME}"

rm -r ${SBIN_DIR_NAME}
is_suc "rm -r ${SBIN_DIR_NAME}"

rm ${APP_NAME}
is_suc "rm ${APP_NAME}"

cur_pwd=`pwd`
is_suc "pwd"

ret=${cur_pwd}/${APP_ZIP_NAME}
echo "success, ${ret}"

exit 0

