#!/bin/bash

. ./env.conf
echo "##########start boar_web_replace.sh##########"

if [ ! -d ${APP_PATCH} ]; then
	mkdir -p ${APP_PATCH}
	is_suc "mkdir -p ${APP_PATCH}"
fi

cp ${SEND_TEMP_PATH}/${APP_NAME} ${APP_PATCH}
is_suc "exec cp ${SEND_TEMP_PATH}/${APP_NAME} ${APP_PATCH}"

cd ${APP_PATCH}
is_suc "exec cd ${APP_PATCH}"

jar -xvf ${APP_NAME}
is_suc "jar -xvf ${APP_NAME}"

rm ${APP_NAME}
is_suc "rm ${APP_NAME}"

echo "##########boar_web_replace.sh SUC##########"



exit 0

