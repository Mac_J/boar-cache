#!/bin/bash

. ./env.conf

sh stop.sh
is_suc "stop app"

sh start.sh
is_suc "start app"

exit 0
