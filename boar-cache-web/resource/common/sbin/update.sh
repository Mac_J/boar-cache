#!/bin/bash

. ./env.conf
echo "##########start update.sh##########"

if [ ! -d ${LOG_APP_HOME} ]; then
	mkdir -p ${LOG_APP_HOME}
	is_suc "mkdir -p ${LOG_APP_HOME}"
fi

if [ ! -d ${APP_PATCH}/${APP_NAME} ]; then
	mkdir -p ${APP_PATCH}/${APP_NAME}
	is_suc "mkdir -p ${APP_PATCH}/${APP_NAME}"
else
	cd ${APP_PATCH}
	is_suc "cd ${APP_PATCH}"
	
	rm -r ${APP_NAME}/*
	is_suc "rm -rf  ${APP_NAME}/*"
fi

cp ${SEND_TEMP_PATH}/${APP_NAME}.zip ${APP_PATCH}/${APP_NAME}
is_suc "cp ${SEND_TEMP_PATH}/${APP_NAME}.zip ${APP_PATCH}/${APP_NAME}"

cd ${APP_PATCH}/${APP_NAME}
is_suc "cd ${APP_PATCH}/${APP_NAME}"

unzip ${APP_NAME}.zip
is_suc "unzip ${APP_NAME}.zip"

rm ${APP_NAME}.zip
is_suc "rm ${APP_NAME}.zip"

echo "##########update.sh SUC##########"



exit 0

