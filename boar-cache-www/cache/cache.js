Cache.cache = {
	db: {
		type: [ 'Standalone', 'Master/Backup', 'Master/Slave', 'Cluster' ],
	},
	shard: {
		standby: [ '冷备', '热备', '主从' ]
	},
	node: {
		type: [ '主节点', '从节点', '热备节点', '冷备节点' ],
		color: [ '#84b9e3','#f9dd34','#3262ba','#222','#eee','#fbcc88','#fb8888' ],
		status: [ '不可用', '只读', '可写' ]
	},
	event: {
		'db.down': '缓存数据库不可用',
		'node.status': '节点状态异常'
	}
}
