$(function() {
	App.cache.validation = {
		'com' : {
			'code' : {
				'empty' : 0,
				'len' : { min : 4, max : 32 },
				'cn_az-09' : 0
			},
			'name' : {
				'empty' : 0,
				'len' : { min : 4, max : 32 },
				'cn_az-09' : 0
			}
		}
	}
});