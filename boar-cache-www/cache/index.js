$(function(){
	App.code = 'cache';
	App.ctx = '/' + App.code;
	
	App.go2 = function(to) {
		switch (to) {
		case '':
		case '#':
		case '#home':
			App.show({
				ctx: 'cache',
				action: 'home'
			}, $.noop, App.main);
			break;
		}
	}
});
