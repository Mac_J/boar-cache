$(function() {
	var ctx = '/cache';
	App.cache = {
		'ctx': ctx,
		'home': {
			url: ctx + '/home.htm'
		},
		'dbIndex': {
			url: ctx + '/db/index.htm'
		},
		'shardIndex': {
			url: ctx + '/shard/index.htm'
		},
		'nodeIndex': {
			url: ctx + '/node/index.htm'
		},
		'nodeInfo': {
			url: ctx + '/node/info.htm'
		},
		'nodeMonitor': {
			url: ctx + '/node/monitor.htm'
		},
		'nodeConsole': {
			url: ctx + '/node/console.htm'
		},
		'apiIndex': {
			url: ctx + '/api/index.htm'
		},
		'apiEdit': {
			url: ctx + '/api/edit.htm'
		}
	}
});